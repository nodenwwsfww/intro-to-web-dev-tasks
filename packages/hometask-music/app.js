import './app.css';
import setupHeader from "./components/header/header";
import setupFooter from "./components/footer/footer";
import setupMain from "./components/main/main";
import { registerPage, renderPage } from './utils/page-manager';
import setupLandingPage from './components/pages/landing/landing';
import setupFeaturePage from './components/pages/feature/feature';
import setupSignupPage from './components/pages/signup/signup';
import {FEATURE_PAGE_ID, LANDING_PAGE_ID, SIGNUP_PAGE_ID} from "./data/pages";

const app = document.querySelector('#app');

registerPage(LANDING_PAGE_ID, setupLandingPage);
registerPage(FEATURE_PAGE_ID, setupFeaturePage);
registerPage(SIGNUP_PAGE_ID, setupSignupPage);

setupHeader(app);

const main = setupMain(app);
renderPage(main, LANDING_PAGE_ID);

setupFooter(app);

