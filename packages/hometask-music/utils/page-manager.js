import {LANDING_PAGE_ID} from "../data/pages";

const pages = {};
let currentPageId = LANDING_PAGE_ID;

export function registerPage(pageId, setupPage) {
  pages[pageId] = setupPage;
}

export function getCurrentPageId() {
  return sessionStorage.getItem('currentPageId') || currentPageId;
}

export function setCurrentPageId(pageId) {
  sessionStorage.setItem('currentPageId', pageId);
  currentPageId = pageId;
}

export function renderPage(parent, pageId) {
  const existsPage = parent.querySelector('.page');
  if (existsPage) {
    existsPage.remove();
  }
  setCurrentPageId(pageId);
  pages[pageId](parent);
  currentPageId = pageId;
  [...document.querySelectorAll('.header-nav__item')].forEach(el => {
    el.classList.remove('not-active');
  });
}
