function createElement(tagName, parent, attributes = {}, content = '') {
  let element;
  try {
        element = document.createElement(tagName);
        Object.keys(attributes).forEach(key => {
            element.setAttribute(key, attributes[key]);
        });
        element.textContent = content;
        parent.appendChild(element);
        return element;
    } catch(error) {
      const errorText = `Error creating element with tag ${tagName} with attributes  ${JSON.stringify(attributes)}. Error: ${error}`;
      throw new Error(errorText);
  }
    return element;
}

export default createElement;
