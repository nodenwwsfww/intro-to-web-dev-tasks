import './header.css'
import createElement from "../../utils/create-element";
import {navLogoItem, navbarItemsByPage} from "../../data/header-items";
import {getCurrentPageId, renderPage} from "../../utils/page-manager";

export default function setupHeader(parent) {
    const header = createElement('header', parent, {
        class: 'header',
    });

    const navbar = createElement('nav', header, {
      class: 'header-nav'
    });

    const navbarList = createElement('ul', navbar, {
      class: 'header-nav__list',
    });

    const navListItem = createElement('li', navbarList, {
      class: 'header-nav__item',
    });

    const headerLogoContainer = createElement('div', navListItem, {
      class: 'logo-container'
    });

    createElement('img', headerLogoContainer, {
      class: 'logo-container__img',
      src: './assets/icons/logo.svg',
    });

    createElement('a', headerLogoContainer, {
      class: 'header-nav__logo-title header-nav__item',
    }, navLogoItem);

    navbarItemsByPage[getCurrentPageId()].items.forEach(item => {
      const itemText = item.item;
      const attributes = {
        class: 'header-nav__item',
        'data-item-text': itemText,
      };
      const navItem = createElement('li', navbarList, attributes)
      createElement('a', navItem, {}, itemText);
    });

    navbarList.addEventListener('click', ({target}) => {
      const navItem = target.closest('.header-nav__item');
      if (navItem) {
        navbarItemsByPage[getCurrentPageId()].items.forEach(item => {
          if (item.item === navItem.dataset.itemText && item?.destinationPageId) {
            renderPage(parent.querySelector('main'), item.destinationPageId);
          }
        });
      }
    });
    return header;
}
