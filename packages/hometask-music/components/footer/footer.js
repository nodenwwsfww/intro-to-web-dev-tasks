import './footer.css'
import createElement from "../../utils/create-element";
import navbarItems from "../../data/footer-items";

export default function setupFooter(parent) {
    const footer = createElement('footer', parent, {
        class: 'footer',
    });


    const navbar = createElement('nav', footer, {
      class: 'nav'
    });


    const navbarList = createElement('ul', navbar, {
      class: 'footer-nav__list',
    });

    navbarItems.forEach(itemText => createElement('li', navbarList, {
      class: 'footer-nav__item',
    }, itemText));

    return footer;
}
