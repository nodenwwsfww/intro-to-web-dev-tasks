import './main.css'
import createElement from "../../utils/create-element";

export default function setupMain(parent) {
    const main = createElement('main', parent);
    return main;
}
