import createElement from "../../../utils/create-element";
import signupImageUrl from "../../../assets/images/background-page-sign-up.png";
import './signup.css';

export default function setupSignupPage(parent) {
  const signup = createElement('div', parent, {
    class: 'signup page',
  });
  const leftContent = createElement('div', signup, {
    class: 'left-content',
  });

  const signupContainer = createElement('div', leftContent, {
    class: 'signup__container',
  });

  const inputFields = ['Name:', 'Password:', 'e-mail:'];

  inputFields.forEach(inputField => {
    const inputFieldContainer = createElement('div', signupContainer, {
      class: 'input-field-container'
    });
    createElement('p', inputFieldContainer, {}, inputField);
    createElement('input', inputFieldContainer, {});
  });

  const callToActionBtn = createElement('button', leftContent, {
    class: 'left-content__button',
  });

  createElement('span', callToActionBtn, {
    class: 'left-content__button-text'
  }, 'Join now');

  createElement('img', signup, {
    src: signupImageUrl,
    class: 'signup__image',
    alt: 'signup image',
  });

  return signup;
}
