import createElement from "../../../utils/create-element";
import './landing.css';
import landingImgUrl from '../../../assets/images/page-landing.png';
import {renderPage} from "../../../utils/page-manager";
import {SIGNUP_PAGE_ID} from "../../../data/pages";

export default function setupLandingPage(parent) {
  const landing = createElement('div', parent, {
    class: 'landing page',
  });

  const leftContent = createElement('div', landing, {
    class: 'left-content',
  });

  createElement('h1', leftContent, {
    class: 'left-content__title',
  }, 'Feel the music');

  createElement('p', leftContent, {
    class: 'left-content__text',
  }, 'Stream over 10 million songs with one click');

  const callToActionBtn = createElement('button', leftContent, {
    class: 'left-content__button',
  });

  createElement('span', callToActionBtn, {
    class: 'left-content__button-text'
    }, 'Join now');

  callToActionBtn.addEventListener('click', () => {
    renderPage(parent, SIGNUP_PAGE_ID);
    [...document.querySelectorAll('.header-nav__item')].find(navItem => navItem.dataset.itemText === 'Join').classList.toggle('not-active');
  });

  createElement('img', landing, {
      src: landingImgUrl,
      class: 'landing__image',
      alt: 'landing image',
    });

    return landing;
}
