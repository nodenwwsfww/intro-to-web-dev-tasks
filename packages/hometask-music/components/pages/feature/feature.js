import createElement from "../../../utils/create-element";
import featurePageUrl from "../../../assets/images/music-titles.png";
import './feature.css';

export default function setupFeaturePage(parent) {
  const feature = createElement('div', parent, {
    class: 'feature page',
  });
  const leftContent = createElement('div', feature, {
    class: 'left-content',
  });

  createElement('h1', leftContent, {
    class: 'left-content__title',
  }, 'Discover new music');

  const buttonsContainer = createElement('div', leftContent, {
    class: 'left-content__buttons-container'
  });
  const buttonsList = ['Charts', 'Songs', 'Artists'];
  buttonsList.forEach(buttonText => {
    const currentButton = createElement('button', buttonsContainer, {
      class: 'left-content__button',
    });
    createElement('span', currentButton, {
      class: 'left-content__button-text'
    }, buttonText);
  });

  createElement('p', leftContent, {
    class: 'left-content__text',
  }, 'By joing you can benefit by listening to the latest albums released');

  createElement('img', feature, {
    src: featurePageUrl,
    class: 'feature__image',
    alt: 'feature image',
  });

  return feature;
}

