import {FEATURE_PAGE_ID, LANDING_PAGE_ID, SIGNUP_PAGE_ID} from "./pages";

export const navLogoItem = 'Simo';
export const navbarItemsByPage = {
  [LANDING_PAGE_ID]: {
    items: [
      {
        item: 'Discover',
        destinationPageId: FEATURE_PAGE_ID,
      },
      {
        item: 'Join',
        destinationPageId: SIGNUP_PAGE_ID,
      },
      {
        item: 'Sign in',
        destinationPageId: SIGNUP_PAGE_ID,
      }
    ],
  },
  [FEATURE_PAGE_ID]: {
    items: [
      {
        item: 'Discover',
      },
      {
        item: 'Join',
        destinationPageId: SIGNUP_PAGE_ID,
      },
      {
        item: 'Sign in',
      }
    ],
  },
  [SIGNUP_PAGE_ID]: {
    items: [
      {
        item: 'Discover',
        destinationPageId: LANDING_PAGE_ID,
      },
      {
        item: 'Join',
      },
      {
        item: 'Sign in',
      }
    ],
  },
}
